const sql = require('mssql');
const dbConfig = require('../config/config');

const config = {
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  server: dbConfig.SERVER,
  database: dbConfig.DATABASE,
  options: dbConfig.OPTIONS
};

const pool = new sql.ConnectionPool(config);
pool.connect(err => {if (err) console.log(err) });

module.exports = pool;
