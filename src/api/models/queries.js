const pool = require('./pool');

const getSeverity = (request, response) => {
    const sqlQuery = 'SELECT * FROM [AdvosBITest].[dbo].[error_severity]';
    const sqlRequest = pool.request();
    sqlRequest.query(sqlQuery, (err, results) => {
      if(err) console.log(err);
      console.log(results.recordset);
      response.status(200).json(results.recordset);
    });
};

const getErrors = (request, response) => {
  const sqlQuery =
    'SELECT  [Error Number (Polarion)]\n' +
    ',[Error-ID]\n' +
    ',[Error Text (User)]\n' +
    ',[Solution for User]\n' +
    ',[Error Text (Service)]\n' +
    ',[Solution for Service]\n' +
    ',[Severity]\n' +
    ',[Abort]\n' +
    ',[Deprecated]\n' +
    'FROM [AdvosBITest].[dbo].[ListOfSystemErrors_deu_complete]';
  const sqlRequest = pool.request();
  sqlRequest.query(sqlQuery, (err, results) => {
    if(err) console.log(err);
    console.log(results.recordset);
    response.status(200).json(results.recordset);
  });
};

const getCatchphrase = (request, response) => {
  const sqlQuery =
    'SELECT [catchphrase_id]\n' +
	  ',[translation]\n' +
	  'FROM [AdvosBITest].[dbo].[catchphrase_translation]\n' +
	  'ORDER BY [translation]';
  const sqlRequest = pool.request();
  sqlRequest.query(sqlQuery, (err, results) => {
    if(err) console.log(err);
    console.log(results.recordset);
    response.status(200).json(results.recordset);
  });
};

module.exports = {getSeverity, getErrors, getCatchphrase};
