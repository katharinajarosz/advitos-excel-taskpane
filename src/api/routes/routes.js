module.exports = app => {
  const db = require("../models/queries");

  app.get('/api/severity', db.getSeverity);
  app.get('/api/errors', db.getErrors);
  app.get('/api/catchphrase', db.getCatchphrase);

}
