import React from 'react'

export const SeverityTable = ({data}) => {

  const TableRow = (item,index) => {

    return(
      <tr key = {index} className={index%2 === 0?'odd':'even'}>
        <td>{item.id}</td>
        <td>{item.internal}</td>
        <td>{item.severity}</td>
      </tr>
    )
  };

  const userTable = data.map((item, index) => TableRow(item,index));

  return(
    <div className="container">
      <h2>Severity</h2>
      <table className="table table-bordered">
        <thead>
        <tr>
          <th>Id</th>
          <th>Internal</th>
          <th>Severity</th>
        </tr>
        </thead>
        <tbody>
        {userTable}
        </tbody>
      </table>
    </div>
  )
};

