import React from 'react'

export const SeverityButton = ({getAllUsers}) => {

  return(
    <div>
      <div className="btn">
        <button type="button" onClick={(e) => getAllUsers()} className="btn btn-warning">Get Severities</button>
      </div>
    </div>
  )
}
