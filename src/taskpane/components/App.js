import * as React from "react";
import {DefaultButton, Stack, Dropdown} from "office-ui-fabric-react";
import Header from "./Header";
import Progress from "./Progress";
// import {ExcelTableUtil} from "../functions/ExcelTableUtil";
import {getSeverity, getErrors, getCatchphrase} from "../services/Service";
import {SeverityButton} from "./SeverityButton";
import {SeverityTable} from "./SeverityTable";
/* global Button, console, Excel, Header, HeroList, HeroListItem, Progress */

const verticalGapStackTokens = {
  childrenGap: 10,
  padding: 10,
};
const dropdownStyles = {
  dropdown: {width: 300}
};

export default class App extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      showTable: false,
      listItems: [],
      dropdownOptions: []
    };
  }

  componentDidMount() {
    this.setDropdown().then( () => {});
  }

  getSeverityData = async () => {
    getSeverity()
      .then(response => {
        Excel.run(async context => {
          let currentWorksheet = context.workbook.worksheets.getActiveWorksheet();
          let severityTable = currentWorksheet.tables.add("A1:C1", true /*hasHeaders*/);
          severityTable.name = "SeverityTable";
          severityTable.getHeaderRowRange().values =
            [["ID", "INTERNAL", "SEVERITY"]];
          const newData = response.map(item => [item.id, item.internal, item.severity]);
          severityTable.rows.add(null /*add at the end*/, newData);
          severityTable.getRange().format.autofitColumns();
          severityTable.getRange().format.autofitRows();
          return context.sync();
        })
      }, err => {
        console.log(err)
      })
  };

  getErrors = async () => {
    getErrors()
      .then(response => {
        Excel.run(async context => {
          let currentWorksheet = context.workbook.worksheets.getActiveWorksheet();
          let errorTable = currentWorksheet.tables.add("A1:I1", true /*hasHeaders*/);
          errorTable.name = "ErrorTable";
          errorTable.getHeaderRowRange().values =
            [["POLARION ID", "ERROR ID", "DESCRIPTION FOR USER", "SOLUTION FOR USER",
              "DESCRIPTION FOR SERVICE", "SOLUTION FOR SERVICE", "SEVERITY", "ABORTION", "DEPRECATED"]];
          const newData = response.map(item => [item.error_id, item.error_symbol, item.error_text_user, item.solution_for_user,
            item.error_text_service, item.solution_for_service, item.severity, item.abortion, item.deprecated]);
          errorTable.rows.add(null /*add at the end*/, newData);
          errorTable.getRange().format.autofitColumns();
          errorTable.getRange().format.autofitRows();
          return context.sync();
        })
      }, err => {
        console.log(err)
      })
  };

  setDropdown = async () => {
    getCatchphrase()
      .then(response => {
        let options = [];
        response.forEach(item => {
          let option = {};
          option.key = item.id;
          option.text = item.translation;
          options.push(option);
        });
        this.setState({dropdownOptions: options});
        // this.setState({showTable: true});
      }, err => {
        console.log(err);
      })
  };

  handleSubmit = async () => {
    getSeverity()
      .then(response => {
        this.setState({listItems: response});
        this.setState({showTable: true});
      }, err => {
        console.log(err);
      })
  };

  render() {
    const {title, isOfficeInitialized} = this.props;

    if (!isOfficeInitialized) {
      return (
        <Progress title={title} logo="assets/logo-filled.png" message="Please sideload your addin to see app body."/>
      );
    }

    return (
      <div className="ms-welcome">
        <Stack tokens={verticalGapStackTokens}>
          <Header
            title={this.props.title}
            logo="assets/logo-filled.png"
            message="Errors"/>
          <DefaultButton
            text="Create Severity Table"
            iconProps={{iconName: "CircleAddition"}}
            onClick={this.getSeverityData.bind(this)}>
          </DefaultButton>
          <DefaultButton
            text="Create Error Table"
            iconProps={{iconName: "CircleAddition"}}
            onClick={this.getErrors.bind(this)}>
          </DefaultButton>
          <Dropdown
          label="Catchphrase"
          multiSelect
          options={this.state.dropdownOptions}
          styles={dropdownStyles}
          />
          <SeverityButton
            getAllUsers={this.handleSubmit.bind(this)}>
          </SeverityButton>
        </Stack>
        <div>{this.state.showTable && <SeverityTable data={this.state.listItems}/>}</div>
      </div>
    );
  }
}
