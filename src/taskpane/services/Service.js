
export async function getSeverity() {
  const response = await fetch(`http://localhost:5000/api/severity`);
  return await response.json();
}

export async function getErrors() {
  const response = await fetch(`http://localhost:5000/api/errors`);
  return await response.json();
}

export async function getCatchphrase() {
  const response = await fetch(`http://localhost:5000/api/catchphrase`);
  return await response.json();
}
